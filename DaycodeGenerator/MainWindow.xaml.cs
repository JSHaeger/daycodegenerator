﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace DaycodeGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DaycodeGeneratorClass.PrintDayCodes();

            lblDateYesterday.Content = string.Format("Yesterday\n{0}", DateTime.Now.AddDays(-1).ToString("MMM d yyyy"));
            lblDateToday.Content = string.Format("Today\n{0}", DateTime.Now.ToString("MMM d yyyy"));
            lblDateTomorrow.Content = string.Format("Tomorrow\n{0}", DateTime.Now.AddDays(1).ToString("MMM d yyyy"));
            
            for (int i = 0; i < 3; i++)
            {
                int dayOffset = i - 1;
                int column = i + 1;
                AddLabel(1, column, DaycodeGeneratorClass.GetDaycode(dayOffset));
                AddLabel(2, column, DaycodeGeneratorClass.GetDayCodeQAPackage(dayOffset));
                AddLabel(3, column, DaycodeGeneratorClass.GetDayCodeMultiStation(dayOffset));
                AddLabel(4, column, DaycodeGeneratorClass.GetDayCodeRobot(dayOffset));
                AddLabel(5, column, DaycodeGeneratorClass.GetDayCodeAirBleed(dayOffset));
            }
        }

        private void AddLabel(int row, int column, string content)
        {
            Label label = new Label
            {
                Content = content,
                VerticalContentAlignment = VerticalAlignment.Center,
            };
            label.MouseLeftButtonUp += DaycodeLabel_MouseLeftButtonUp;
            Grid.SetRow(label, row);
            Grid.SetColumn(label, column);
            gridDaycode.Children.Add(label);
        }

        private void DaycodeLabel_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Clipboard.SetText(((Label)sender).Content.ToString());
        }
    }
}
