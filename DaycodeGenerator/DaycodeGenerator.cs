﻿using System;

namespace DaycodeGenerator
{
    public static class DaycodeGeneratorClass
    {
        public static void PrintDayCodes()
        {
            Console.WriteLine("HaegerTechnician: {0}", GetDaycode());
            Console.WriteLine("DayCodeQAPackage: {0}", GetDayCodeQAPackage());
            Console.WriteLine("DayCodeMultiStation: {0}", GetDayCodeMultiStation());
            Console.WriteLine("DayCodeRobot: {0}", GetDayCodeRobot());
            Console.WriteLine("DayCodeAirBleed: {0}", GetDayCodeAirBleed());
        }

        public static string GetDaycode(int dayOffset = 0, int multiplier = 37)
        {
            DateTime now = DateTime.Now.AddDays(dayOffset);

            if (now.Day == 1)
            {
                now = now.AddDays(-1);
            }

            int day = now.Day;
            int month = now.Month;
            int year = now.Year;

            int n1 = (month * day) + year;
            int n2 = (day * year) + month;
            int n3 = n1 ^ n2;

            return (n3 * multiplier).ToString();
        }

        public static string GetDayCodeQAPackage(int dayOffset = 0)
        {
            return GetDaycode(dayOffset, 40);
        }

        public static string GetDayCodeMultiStation(int dayOffset = 0)
        {
            return GetDaycode(dayOffset, 39);
        }

        public static string GetDayCodeRobot(int dayOffset = 0)
        {
            return GetDaycode(dayOffset, 38);
        }

        public static string GetDayCodeAirBleed(int dayOffset = 0)
        {
            return GetDaycode(dayOffset, 54);
        }
    }
}
